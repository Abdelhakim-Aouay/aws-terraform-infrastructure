provider "aws" {
  region = "eu-west-3"
}

variable "cidr_block" {}
variable "env-prefix" {}
variable "subnet-cidr-block" {}
variable "avail_zone" {}
variable "my-ip" {}
variable "instance_type" {}

resource "aws_vpc" "myapp_vpc" {
  cidr_block = var.cidr_block

  tags = {
    Name = "${var.env-prefix}-vpc"
  }
}

resource "aws_subnet" "my_subnet-1" {
  vpc_id            = aws_vpc.myapp_vpc.id
  cidr_block        = var.subnet-cidr-block
  availability_zone = var.avail_zone

  tags = {
    Name = "${var.env-prefix}-subnet-1"
  }
}

# creation oof our route table and associate with the subnet
/*resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_vpc.myapp_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-gw.id
  }
  tags = {
    Name = "${var.env-prefix}-myapp-route-table"
  }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id      = aws_subnet.my_subnet-1.id
  route_table_id = aws_route_table.myapp-route-table.id
}*/

resource "aws_internet_gateway" "myapp-gw" {
  vpc_id = aws_vpc.myapp_vpc.id

  tags = {
    Name = "${var.env-prefix}-myapp-gw"
  }
}
# on utulise default route table and associate to subnet
resource "aws_default_route_table" "main-rtb" {
  default_route_table_id = aws_vpc.myapp_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-gw.id
  }
  tags = {
    Name = "${var.env-prefix}-main-route-table"
  }

}

resource "aws_default_security_group" "default-sg" {

  vpc_id = aws_vpc.myapp_vpc.id

  ingress {
    description = "allow ssh port"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my-ip]
    #ipv6_cidr_blocks = [aws_vpc.myapp_vpc.ipv6_cidr_block]
  }
  ingress {
    description = "allow jenkins port"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = [aws_vpc.myapp_vpc.ipv6_cidr_block]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${var.env-prefix}-default-sg"
  }
}

resource "aws_instance" "myapp-myec2" {
  ami                    = "ami-00983e8a26e4c9bd9"
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_default_security_group.default-sg.id]
  subnet_id              = aws_subnet.my_subnet-1.id
  key_name               = aws_key_pair.TF_Key.key_name
  availability_zone      = var.avail_zone

  associate_public_ip_address = true

  tags = {
    Name = "${var.env-prefix}-server"
  }
}

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "TF_Key" {
  key_name   = "TF_key"
  public_key = tls_private_key.rsa.public_key_openssh
}

resource "local_file" "TF_key" {
  content  = tls_private_key.rsa.private_key_pem
  filename = "tfkey"
}

output "instance_id" {
  description = "ip public of the EC2 instance"
  value       = aws_instance.myapp-myec2.public_ip
}